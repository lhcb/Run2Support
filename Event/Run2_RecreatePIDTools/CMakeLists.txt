###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/RecreatePIDTools
----------------------
#]=======================================================================]

# Note that we prefix the module name with Run2_ to distinguish from the
# module in LHCb (names must be unique for a super project build).
gaudi_add_module(Run2_RecreatePIDTools
    SOURCES
        src/ChargedProtoCombineDLLsAlg.cpp
    LINK
        Gaudi::GaudiAlgLib
        LHCb::RecEvent
)
