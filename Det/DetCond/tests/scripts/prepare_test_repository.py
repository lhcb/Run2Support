#!/usr/bin/env python
###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# -*- coding: utf-8 -*-
'''
Prepare the Git repository to test DD4hep conditions.
'''
from __future__ import print_function

import sys
import os
from datetime import datetime
from subprocess import check_call, check_output, STDOUT
from os.path import join, isdir, dirname, exists, abspath
from shutil import copytree, rmtree, copy

EPOCH = datetime(1970, 1, 1)


def echo_cmd(func):
    def wrap(*args, **kwargs):
        print(args, kwargs)
        return func(*args, **kwargs)

    return wrap


check_call = echo_cmd(check_call)
check_output = echo_cmd(check_output)


def makedirs(*args):
    '''
    Create requested directories, if needed.
    '''
    for path in args:
        if not isdir(path):
            os.makedirs(path)


def to_ts(dt):
    return int((dt - EPOCH).total_seconds() * 1e9)


def write_IOVs(iovs, path):
    '''
    write a list of (datetime, key) pairs to the IOVs file in path.
    '''
    with open(join(path, 'IOVs'), 'w') as IOVs:
        IOVs.write(''.join(
            '{0} {1}\n'.format(to_ts(dt), key) for dt, key in iovs))


# first argument is destination directory
path = sys.argv[1] if len(sys.argv) > 1 else os.environ['GIT_TEST_REPOSITORY']

if exists(path):
    rmtree(path)

# initialize repository from template (tag 'v0')
src_data = join(
    dirname(dirname(__file__)), 'data', 'DD4TESTCOND', 'Conditions', 'test',
    'conditions.yml')
cond_path = join(path, 'Conditions', 'test', 'conditions.yml')
copytree(src_data, cond_path)

check_output(['git', 'init', '--quiet', path])
check_call(['git', 'config', '-f', '.git/config', 'user.name', 'Test User'],
           cwd=path)
check_call(
    ['git', 'config', '-f', '.git/config', 'user.email', 'test.user@no.where'],
    cwd=path)
check_call(['git', 'add', '.'], cwd=path)
env = dict(os.environ)
env['GIT_COMMITTER_DATE'] = env['GIT_AUTHOR_DATE'] = '1483225200'
check_call(['git', 'commit', '-m', 'initial version'], cwd=path, env=env)
check_call(['git', 'tag', 'v0'], cwd=path)

with open(join(cond_path, "v1.yml"), "w") as f:
    f.write("""
TestCondition: !testcond
  par1: 100.6
  par2: 101.7
  parv: [4.5, 5.6]
TestConditionYML:
  par1: 100.6
  par2: 101.7
  parv: [4.5, 5.6]
""")

check_call(['git', 'add', '--all', 'Conditions/test/conditions.yml'], cwd=path)
check_call(['git', 'commit', '-am', 'v1 data'], cwd=path)
check_call(['git', 'tag', 'v1'], cwd=path)

# make a "bare" clone of the repository
if exists(path + '-bare.git'):
    rmtree(path + '-bare.git')
print(check_output(['git', 'clone', '--mirror', path, path + '-bare.git'],
                   stderr=STDOUT))
