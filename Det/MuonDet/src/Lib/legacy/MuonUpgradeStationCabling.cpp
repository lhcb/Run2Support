/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonDet/MuonUpgradeStationCabling.h"

#include <iostream>

//-----------------------------------------------------------------------------
// Implementation file for class : MuonUpgradeStationCabling
//
// 2019-07-07 : Alessia Satta
//-----------------------------------------------------------------------------

StatusCode MuonUpgradeStationCabling::initialize() {
  const auto tellName = this->paramVect<std::string>( "TellName" );
  // std::cout<<" size "<< tellName.size()<<endmsg;

  // std::cout<<"ciao init "<< m_numberOfTell40Board<<std::endl;
  m_listOfTell40.clear();

  for ( auto tname = tellName.begin(); tname < tellName.end(); tname++ ) {
    auto sc = addTell40Name( *tname );
    if ( sc.isFailure() ) { return sc; }

    addTell40Name( *tname ).ignore();

    m_numberOfTell40Board++;
  }

  //<paramVector name="TellName" type="string">
  return StatusCode::SUCCESS;
}

StatusCode MuonUpgradeStationCabling::update( long l1numb ) {
  m_numberOfTell40Board = l1numb;
  return StatusCode::SUCCESS;
}

StatusCode MuonUpgradeStationCabling::addTell40Name( std::string name ) {
  if ( static_cast<int>( m_listOfTell40.size() ) > m_numberOfTell40Board ) return StatusCode::FAILURE;
  m_listOfTell40.push_back( name );
  return StatusCode::SUCCESS;
}

/// update constructor, do a deep copy of all
/// except for the properties of a generic DataObject

void MuonUpgradeStationCabling::update( Condition& obj ) {
  // std::cout<<"ciao 1"<<std::endl;
  Condition::update( obj );
  const MuonUpgradeStationCabling& obj1 = static_cast<const MuonUpgradeStationCabling&>( obj );
  m_numberOfTell40Board                 = obj1.getNumberOfTell40Board();
  m_listOfTell40                        = obj1.getAllTell40Names();
  m_numberOfTell40Board                 = 95;
}

/// update constructor, do a deep copy of all
/// except for the properties of a generic DataObject
void MuonUpgradeStationCabling::update( ValidDataObject& obj ) {
  // std::cout<<"ciao2 "<<std::endl;
  Condition& cc = dynamic_cast<Condition&>( obj );
  Condition::update( cc );
  MuonUpgradeStationCabling& obj1 = static_cast<MuonUpgradeStationCabling&>( obj );
  m_numberOfTell40Board           = obj1.getNumberOfTell40Board();
  m_listOfTell40                  = obj1.getAllTell40Names();
}
