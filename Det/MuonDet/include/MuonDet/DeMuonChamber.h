/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#pragma once

#include "MuonDet/MuonNamespace.h"

#include "DetDesc/DetectorElement.h"

#include "fmt/format.h"

#include <memory>
#include <sstream>
#include <string>
#include <vector>

/// Class ID of chambers
static const CLID CLID_DEMuonChamber = 11006;

/** @class DeMuonChamber DeMuonChamber.h MuonDet/DeMuonChamber.h
 *
 *  Detector element class for a single chamber in the muon system
 *
 *  @author David Hutchcroft
 *  @date   21/01/2002
 */
class DeMuonChamber : public DetDesc::DetectorElementPlus {

public:
  /// Constructor (empty)
  DeMuonChamber() = default;

  /// Constructor used by XmlMuonRegionCnv to create chambers
  /// pad sizes in mm
  DeMuonChamber( int nStation, int nRegion, int nChamber );

  static const CLID& classID() { return CLID_DEMuonChamber; }

  const CLID& clID() const override { return classID(); }

  // Initialize
  StatusCode initialize() override;

  /// get Station Number
  inline int stationNumber() const { return m_StationNumber; }

  /// set Station Number (from 0 -> nStations)
  void setStationNumber( int nStation ) { m_StationNumber = nStation; }

  /// get Region Number
  int regionNumber() const { return m_RegionNumber; }

  /// set Region Number
  void setRegionNumber( int nRegion ) { m_RegionNumber = nRegion; }

  /// get Chamber Number
  int chamberNumber() const { return m_ChamberNumber; }

  /// set Chamber Number
  void setChamberNumber( int nChamber ) { m_ChamberNumber = nChamber; }

  /// get chamber Grid
  const std::string& getGridName() const { return m_chmbGrid; }

  /// set chamber Grid
  void setGridName( std::string_view grid ) { m_chmbGrid = grid; }

  /// check that hit is inside chamber but also gap
  bool checkHitAndGapInChamber( float xpos, float ypos ) const;

  /// return true in case of success, false otherwise
  bool  calculateHitPosInGap( int gapNumber, float xpos, float ypos, float xSlope, float ySlope, float averageZ,
                              Gaudi::XYZPoint& entryGlobal, Gaudi::XYZPoint& exitGlobal ) const;
  float calculateAverageGap( int gapNumberStart, int gapNumberStop, float xpos, float ypos ) const;

  std::tuple<ROOT::Math::XYZPoint, ROOT::Math::XYZPoint> getGapPoints( int gap, double x, double y ) const;

  int getGasGapNumber() const;

  IPVolume* getFirstGasGap() const;
  IPVolume* getGasGap( int number ) const;

  struct PointInGasGap {
    Gaudi::XYZPoint pointInGap;
    int             number;
    IPVolume*       gasVolume;
  };
  std::optional<PointInGasGap> isPointInGasGap( Gaudi::XYZPoint pointInChamber ) const;
  IPVolume*                    getGasGapLayer( int number ) const;

private:
  /// Chamber Grid
  std::string m_chmbGrid;

  /// Station number
  int m_StationNumber = 0;

  /// Region number in station
  int m_RegionNumber = 0;

  /// Chamber number in region
  int m_ChamberNumber = 0;
};
