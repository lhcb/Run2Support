###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from PyConf.application import (configure_input, configure, ApplicationOptions,
                                default_raw_event)
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Algorithms import (LHCb__DetDesc__TestBeamSpot as TestBeamSpot,
                               LHCb__Tests__FakeRunNumberProducer as
                               FakeRunNumber)
from PRConfig.TestFileDB import test_file_db
from Configurables import MessageSvc, UpdateManagerSvc

options = ApplicationOptions(_enabled=False)

# Not data from the input is used, but something should be there
options.input_files = [
    "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/upgrade-magup-sim10aU1-minbias/Digi-00151659_00000001_dddb-20220705_sim-20220705-vc-mu100.digi"
]
options.input_type = 'ROOT'
options.data_type = 'Upgrade'
options.dddb_tag = 'upgrade/dddb-20220705'
options.conddb_tag = 'upgrade/sim-20220705-vc-mu100'
options.simulation = True

options.velo_motion_system_yaml = os.path.expandvars(
    '${VPDETROOT}/tests/data/DB/updates/yaml/Conditions/VP/Motion.yml')
options.evt_max = 8
config = configure_input(options)

MessageSvc().setVerbose += ["VeloAlignCond"]


def fake_odin():
    return FakeRunNumber(Start=0, Step=1).ODIN


test_beamspot = TestBeamSpot()

cf_node = CompositeNode(
    "velo_decoding", [fake_odin().producer, test_beamspot],
    combine_logic=NodeLogic.LAZY_AND,
    force_order=True)

config.update(configure(options, cf_node, make_odin=fake_odin))
