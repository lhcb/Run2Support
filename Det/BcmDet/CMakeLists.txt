###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Det/BcmDet
----------
#]=======================================================================]

gaudi_add_library(BcmDetLib
    SOURCES
        src/Lib/DeBcm.cpp
        src/Lib/DeBcmSens.cpp
    LINK
        PUBLIC
            Run2Support::DetDescLib
)

gaudi_add_module(BcmDet
    SOURCES
        src/component/BcmDetFactories.cpp
    LINK
        Run2Support::BcmDetLib
        Run2Support::DetDescCnvLib
)

gaudi_add_dictionary(BcmDetDict
    HEADERFILES dict/BcmDetDict.h
    SELECTION dict/BcmDetDict.xml
    LINK Run2Support::BcmDetLib
)
