/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//----------------------------------------------------------------------------
/** @file DeRichLocations.h
 *
 *  Header file that defines the DDDB locations of the various RICH
 *  detector elements, and various utility methods to help access these
 *  locations
 *
 *  @author Antonis Papanestis a.papanestis@rl.ac.uk
 *  @author  Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date   2006-03-02
 */
//----------------------------------------------------------------------------

#pragma once

// LHCbKernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichRadiatorType.h"

// RichDet
#include "RichDet/RichDetConfigType.h"

// RichUtils
#include "RichUtils/RichException.h"

class DeRich;
class DeRich1;
class DeRich2;
class DeRichRadiator;

// STL
#include <string>
#include <type_traits>
#include <vector>

// *******************************************************************************************

/** @namespace DeRichLocations
 *
 *  Namespace for the location of Rich Detector Elements in xml
 *
 *  @author Antonis Papanestis a.papanestis@rl.ac.uk
 *  @date   2004-06-18
 */
namespace DeRichLocations {

  // ----------------------------------------------------------------------------------------
  // Location Strings
  // ----------------------------------------------------------------------------------------

  /// Rich1 location in transient detector store
  inline const std::string OldRich1 = "/dd/Structure/LHCb/BeforeMagnetRegion/Rich1";
  inline const std::string Rich1    = OldRich1;

  /// Rich2 location in transient detector store
  inline const std::string OldRich2 = "/dd/Structure/LHCb/AfterMagnetRegion/Rich2";
  inline const std::string Rich2    = OldRich2;

  /// RichSystem location in transient detector store
  inline const std::string RichSystem = "/dd/Structure/LHCb/AfterMagnetRegion/Rich2/RichSystem";

  /// Location of Rich1 top panel
  inline const std::string Rich1TopPanel = "/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PDPanel0";
  inline const std::string Rich1Panel0   = Rich1TopPanel;

  /// Location of Rich1 bottom panel
  inline const std::string Rich1BottomPanel = "/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PDPanel1";
  inline const std::string Rich1Panel1      = Rich1BottomPanel;

  /// Location of Rich2 left panel
  inline const std::string Rich2LeftPanel = "/dd/Structure/LHCb/AfterMagnetRegion/Rich2/RichSystem/HPDPanel0";
  inline const std::string Rich2Panel0    = Rich2LeftPanel;

  /// Location of Rich2 right panel
  inline const std::string Rich2RightPanel = "/dd/Structure/LHCb/AfterMagnetRegion/Rich2/RichSystem/HPDPanel1";
  inline const std::string Rich2Panel1     = Rich2RightPanel;

  /// Multi solid aerogel location
  inline const std::string Aerogel = "/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Aerogel";

  /// Rich1 gas (C4F10) location
  inline const std::string OldRich1Gas = "/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Gas";
  inline const std::string Rich1Gas    = OldRich1Gas;
  inline const std::string C4F10       = Rich1Gas;

  /// Rich2 gas (CF4) location
  inline const std::string OldRich2Gas = "/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2Gas";
  inline const std::string Rich2Gas    = OldRich2Gas;
  inline const std::string CF4         = Rich2Gas;

  /// Rich1 Beampipe location in TDS
  inline const std::string Rich1BeamPipe = "/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1BeamPipe";
  /// Rich2 Beampipe location in TDS
  inline const std::string Rich2BeamPipe = "/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2BeamPipe";

  /// Detector numbering locations
  inline std::vector<std::string> detectorNumberings( const Rich::RichPhDetConfigType type ) noexcept {
    if ( Rich::PMTConfig == type ) { return {"Rich1PMTDetectorNumbers", "Rich2PMTDetectorNumbers"}; }
    if ( Rich::HPDConfig == type ) { return {"Rich1DetectorNumbers", "Rich2DetectorNumbers"}; }
    return {};
  }

  /// Inactive PD condition locations
  inline std::vector<std::string> inactivePDs() noexcept { return {"Rich1InactivePDs", "Rich2InactivePDs"}; }

  // PMT channel properties condition location
  inline const std::string PMTPropertiesCondName = "PMTProperties";
  // name of the parameter containing the information on SIN
  inline const std::string PMTSinInfoParamName = "SINRatio";

  // global RICH properties with PMTs locations
  inline const std::string ReadoutTimeInfoCondPath = "/dd/Conditions/HardwareProperties/Rich1/ReadoutTimeInfo";
  inline const std::string ChargeSharingAndCrossTalkCondName = "ChargeSharingAndCrossTalk";
  inline const std::string PMTHighVoltageTabPropLoc          = "/dd/Materials/RichMaterialTabProperties/PmtHighVoltage";

  // ----------------------------------------------------------------------------------------
  // Utility methods
  // ----------------------------------------------------------------------------------------

  /** Gives the DDDB location of the detector element for the given radiator type and detector description type
   *  @param rad The radiator type
   *  @return The DDDB location of the detector element
   */
  template <typename DETELEM>
  inline constexpr auto location( const Rich::RadiatorType rad ) {
    if constexpr ( std::is_base_of_v<DeRichRadiator, DETELEM> ) {
      // Old RichDet
      if ( Rich::Rich1Gas == rad ) { return DeRichLocations::OldRich1Gas; }
      if ( Rich::Rich2Gas == rad ) { return DeRichLocations::OldRich2Gas; }
    }
    throw Rich::Exception( "No Detector Element for radiator type " + Rich::text( rad ) );
    return std::string{"UNDEFINEDLOCATION"};
  }
  /// Returns the location for the default detector description
  const std::string location( const Rich::RadiatorType rad );

  /** Gives the DDDB location of the detector element for the given radiator type
   *  @param det The RICH detector type
   *  @return The DDDB location of the detector element
   */
  template <typename DETELEM>
  inline constexpr auto location( const Rich::DetectorType det ) {
    if constexpr ( std::is_base_of_v<DeRich, DETELEM> ) {
      // Old RichDet
      if ( Rich::Rich1 == det ) { return DeRichLocations::OldRich1; }
      if ( Rich::Rich2 == det ) { return DeRichLocations::OldRich2; }
    }
    throw Rich::Exception( "No Detector Element for detector type " + Rich::text( det ) );
    return std::string{"UNDEFINEDLOCATION"};
  }
  /// Returns the location for the default detector description
  const std::string location( const Rich::DetectorType det );

  /// Use template types only to get access to correct detector location.
  /// Works when there is a specific type for each (like Rich1, Rich2).
  template <typename DETELEM>
  inline constexpr auto location() {
    if constexpr ( std::is_same_v<::DeRich1, DETELEM> ) { return OldRich1; }
    if constexpr ( std::is_same_v<::DeRich2, DETELEM> ) { return OldRich2; }
    // if get here intentional fail to compile ....
  }

  /// Constructs the tag for a given derived condition attached to the given radiator
  template <typename DETELEM>
  inline constexpr auto derivedCondition( const Rich::RadiatorType rad, const std::string& tag ) {
    if constexpr ( std::is_base_of_v<DeRichRadiator, DETELEM> ) {
      // Old RichDet
      if ( Rich::Rich1Gas == rad ) { return "Rich1Gas" + tag; }
      if ( Rich::Rich2Gas == rad ) { return "Rich2Gas" + tag; }
    }
    throw Rich::Exception( "No Detector Element for radiator type " + Rich::text( rad ) );
    return std::string{"UNDEFINEDTAG"};
  }

  /// Constructs the tag for a derived condition attached to the given type
  template <typename DETELEM>
  inline constexpr auto derivedCondition( const std::string& tag ) {
    if constexpr ( std::is_same_v<::DeRich1, DETELEM> ) { return "Rich1" + tag; }
    if constexpr ( std::is_same_v<::DeRich2, DETELEM> ) { return "Rich2" + tag; }
    // if get here intentional fail to compile ....
  }

  /// Constructs the tag for a condition not attached to a paticular RICH entity
  inline auto derivedCondition( const std::string& tag ) { return tag; }

} // namespace DeRichLocations

// ************************************************************************************************************
