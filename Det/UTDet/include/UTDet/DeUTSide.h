/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/UT/ChannelID.h"
#include "GaudiKernel/MsgStream.h"
#include "UTDet/DeUTBaseElement.h"
#include <string>
#include <vector>

class DeUTLayer;
class DeUTDetector;

/** @class DeUTSide DeUTSide.h UTDet/DeUTSide.h
 *
 *  UT Side detector element
 *
 *  @author Xuhao Yuan (based on code by Andy Beiter, Jianchun Wang, Matt Needham)
 *  @date   2021-03-29
 *
 */

static const CLID CLID_DeUTSide = 9302;

class DeUTSide : public DeUTBaseElement {

public:
  /** child type **/
  using child_type = DeUTLayer;

  /** children **/
  using Children = std::vector<child_type const*>;

  /** Constructor **/
  using DeUTBaseElement::DeUTBaseElement;

  /**
   * Retrieves reference to class identifier
   * @return the class identifier for this class
   **/
  static const CLID& classID() { return CLID_DeUTSide; }

  /**
   * another reference to class identifier
   * @return the class identifier for this class
   **/
  const CLID& clID() const override;

  /** initialization method
   * @return Status of initialisation
   **/
  StatusCode initialize() override;

  /** side identifier
   *  @return identifier
   **/
  unsigned int id() const { return m_id; }

  auto version() const { return m_version; }

  /** print to stream */
  std::ostream& printOut( std::ostream& os ) const override;

  /** print to stream */
  MsgStream& printOut( MsgStream& os ) const override;

  /** check contains channel
   *  @param  aChannel channel
   *  @return bool
   **/
  bool contains( const LHCb::Detector::UT::ChannelID aChannel ) const override {
    bool isContain = ( elementID().side() == aChannel.side() );
    if ( m_version == GeoVersion::v0 ) { isContain = ( id() == aChannel.station() ); }
    return isContain;
  }

  /**
   * Nickname for the side
   **/
  const std::string& nickname() const { return m_nickname; }

  /**  locate the layer based on a channel id
   *   @return  layer
   **/
  const DeUTLayer* findLayer( const LHCb::Detector::UT::ChannelID aChannel ) const;

  /** locate layer based on a point
   *  @return layer
   **/
  const DeUTLayer* findLayer( const Gaudi::XYZPoint& point ) const;

  /** vector of children */
  const Children& layers() const { return m_layers; }

  /**
   * fraction active channels
   * @return bool fraction active
   **/
  double fractionActive() const;

  /** ouput operator for class DeUTSide
   *  @see DeUTSide
   *  @see MsgStream
   *  @param os       reference to STL output stream
   *  @param aSide reference to DeUTSide object
   **/
  friend std::ostream& operator<<( std::ostream& os, const DeUTSide& aSide ) { return aSide.printOut( os ); }

  /** ouput operator for class DeUTSide
   *  @see DeUTSide
   *  @see MsgStream
   *  @param os       reference to MsgStream output stream
   *  @param aSide reference to DeUTSide object
   **/
  friend MsgStream& operator<<( MsgStream& os, const DeUTSide& aSide ) { return aSide.printOut( os ); }

private:
  std::string  m_nickname;
  unsigned int m_id      = 0u;
  GeoVersion   m_version = GeoVersion::v1;
  Children     m_layers;
};

[[deprecated( "please deref first" )]] inline std::ostream& operator<<( std::ostream& os, const DeUTSide* aSide ) {
  return os << *aSide;
}
[[deprecated( "please deref first" )]] inline MsgStream& operator<<( MsgStream& os, const DeUTSide* aSide ) {
  return os << *aSide;
}
