/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/UT/ChannelID.h"
#include "UTDet/DeUTBaseElement.h"
#include "UTDet/DeUTLayer.h"
#include "UTDet/DeUTStave.h"
#include <string>
#include <vector>

class DeUTModule;
class DeUTSector;

/** @class DeUTFace DeUTFace.h UTDet/DeUTFace.h
 *
 *  Class representing a UT Stave Face *
 *  @author Xuhao Yuan
 *  @date   2021-03-29
 *
 */

static const CLID CLID_DeUTFace = 9311;

class DeUTFace : public DeUTBaseElement {

public:
  /** parent type */
  using parent_type = DeUTStave;

  /** child type */
  using module_type = DeUTModule;
  using sector_type = DeUTSector;

  /** children */
  using Children  = std::vector<module_type const*>;
  using GChildren = std::vector<sector_type const*>;

  /** Constructor */
  using DeUTBaseElement::DeUTBaseElement;

  /**
   * Retrieves reference to class identifier
   * @return the class identifier for this class
   */
  static const CLID& classID() { return CLID_DeUTFace; }

  /**
   * another reference to class identifier
   * @return the class identifier for this class
   */
  const CLID& clID() const override;

  /** initialization method
   * @return Status of initialisation
   */
  StatusCode initialize() override;

  const std::string& stavetype() const { return m_type; }

  const std::string& staveRotZ() const { return m_staveRotZ; }

  /** first readout sector on Stave
   * @return m_firstSector
   */
  unsigned int firstSector() const { return m_firstSector; }

  /** number of readout sectors expected
   * @return m_numSectors
   */
  unsigned int numSectorsExpected() const { return m_parent->numSectorsExpected() / 2u; }

  /** last readout sector on Stave
   * @return m_firstSector
   */
  unsigned int lastSector() const { return m_firstSector + numSectorsExpected() - 1u; }

  /** test whether contains channel
   * @param  aChannel test channel
   * @return bool
   */
  bool contains( const LHCb::Detector::UT::ChannelID aChannel ) const override;

  /** print to stream */
  std::ostream& printOut( std::ostream& os ) const override;

  /** print to stream */
  MsgStream& printOut( MsgStream& os ) const override;

  /**  locate sector based on a channel id
  @return  sector */
  const DeUTSector* findSector( const LHCb::Detector::UT::ChannelID aChannel ) const;

  /** locate sector based on a point
  @return sector */
  const DeUTSector* findSector( const Gaudi::XYZPoint& point ) const;

  /**  locate module based on a channel id
  @return  module */
  const DeUTModule* findModule( const LHCb::Detector::UT::ChannelID aChannel ) const;

  /** locate module based on a point
  @return module */
  const DeUTModule* findModule( const Gaudi::XYZPoint& point ) const;

  /** children */
  const DeUTFace::Children&  modules() const { return m_modules; }
  const DeUTFace::GChildren& sectors() const { return m_sectors; }

  /** column number */
  unsigned int column() const { return m_parent->column(); }

  /** stave number */
  unsigned int stave() const { return m_parent->stave(); }

  /** production id */
  unsigned int prodID() const { return m_parent->prodID(); }

  /**
   * Nickname for the face
   **/
  const std::string& nickname() const { return m_nickname; }

  /**
   * fraction active channels
   * @return bool fraction active
   */
  double fractionActive() const;

  /** version */
  const std::string& versionString() const { return m_versionString; }

  /** output operator for class DeUTFace
   *  @see DeUTFace
   *  @see MsgStream
   *  @param os      reference to STL output stream
   *  @param aStave reference to DeUTFace object
   */
  friend std::ostream& operator<<( std::ostream& os, const DeUTFace& aStave ) { return aStave.printOut( os ); }

  /** output operator for class DeUTFace
   *  @see DeUTFace
   *  @see MsgStream
   *  @param os      reference to MsgStream output stream
   *  @param aStave reference to DeUTFace object
   */
  friend MsgStream& operator<<( MsgStream& os, const DeUTFace& aStave ) { return aStave.printOut( os ); }

  /**
   * Geometry version
   */
  auto version() const { return m_version; }

private:
  StatusCode updateProdIDCondition();

private:
  unsigned int m_detRegion   = 0;
  unsigned int m_firstSector = 0;
  unsigned int m_column      = 0;
  unsigned int m_face        = 0;
  std::string  m_type;
  std::string  m_staveRotZ;
  unsigned int m_numSectors = 0;
  parent_type* m_parent     = nullptr;
  Children     m_modules;
  GChildren    m_sectors;
  unsigned int m_prodID        = 0;
  std::string  m_versionString = "DC11";
  std::string  m_prodIDString  = "ProdID";
  GeoVersion   m_version       = GeoVersion::ErrorVersion;
  unsigned int m_staveID       = 0u;
  unsigned int m_sideID        = 0u;
  unsigned int m_layerID       = 0u;
  std::string  m_sideString, m_layerString;
  std::string  m_nickname;
};

inline bool DeUTFace::contains( const LHCb::Detector::UT::ChannelID aChannel ) const {
  if ( m_version == GeoVersion::v1 ) {
    return ( aChannel.face() == m_face && aChannel.module() < 2 && aChannel.sector() < 2 ) &&
           m_parent->contains( aChannel );
  } else
    return false;
}

[[deprecated( "first deref" )]] inline std::ostream& operator<<( std::ostream& os, const DeUTFace* aStave ) {
  return os << *aStave;
}
[[deprecated( "first deref" )]] inline MsgStream& operator<<( MsgStream& os, const DeUTFace* aStave ) {
  return os << *aStave;
}
