###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Det/VeloDet
-----------
#]=======================================================================]

if (USE_DD4HEP)
    set(DetDescLocation Run2Support)
else()
    set(DetDescLocation LHCb)
endif()

gaudi_add_library(VeloDetLib
    SOURCES
        src/Lib/DeVelo.cpp
        src/Lib/DeVeloPhiType.cpp
        src/Lib/DeVeloRType.cpp
        src/Lib/DeVeloSensor.cpp
        src/Lib/VeloAlignCond.cpp
        src/Lib/CircleTraj.cpp
        LINK
        PUBLIC
            Gaudi::GaudiKernel
            ${DetDescLocation}::DetDescLib
            LHCb::LHCbKernel
            VDT::vdt
        PRIVATE
            LHCb::LHCbMathLib
)

gaudi_add_module(VeloDet
    SOURCES
        src/component/VeloDetChecker.cpp
        src/component/XmlDeVeloCnv.cpp
        src/component/XmlDeVeloPhiTypeCnv.cpp
        src/component/XmlDeVeloRTypeCnv.cpp
        src/component/XmlVeloAlignCondCnv.cpp
    LINK
        Boost::headers
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        ${DetDescLocation}::DetDescCnvLib
        LHCb::LHCbKernel
        VeloDetLib
)

gaudi_add_dictionary(VeloDetDict
    HEADERFILES dict/VeloDetDict.h
    SELECTION dict/VeloDetDict.xml
    LINK VeloDetLib
)

if(BUILD_TESTING)
    # Prepare test Git CondDB overlay
    file(REMOVE_RECURSE ${CMAKE_CURRENT_BINARY_DIR}/test/DB)
    file(COPY tests/data/DB/simple/ DESTINATION test/DB/simple/)
    file(COPY tests/data/DB/simple/ DESTINATION test/DB/updates/)
    file(REMOVE
        ${CMAKE_CURRENT_BINARY_DIR}/test/DB/updates/Conditions/Velo/Alignment/Global.xml
        ${CMAKE_CURRENT_BINARY_DIR}/test/DB/updates/Conditions/online.xml)
    file(COPY tests/data/DB/updates/ DESTINATION test/DB/updates/)
    execute_process(COMMAND git init -q ${CMAKE_CURRENT_BINARY_DIR}/test/DB/simple)
    execute_process(COMMAND git init -q ${CMAKE_CURRENT_BINARY_DIR}/test/DB/updates)

    gaudi_add_tests(QMTest)

    set_property(
        TEST
            VeloDet.veloaligncond.basic
            VeloDet.veloaligncond.override1
            VeloDet.veloaligncond.override2
            VeloDet.veloaligncond.override3
            VeloDet.veloaligncond.updates
        APPEND PROPERTY 
            ENVIRONMENT TEST_OVERLAY_ROOT=${CMAKE_CURRENT_BINARY_DIR}/test/DB
    )
endif()
