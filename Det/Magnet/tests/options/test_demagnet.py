#####################################################################################
# (c) Copyright 1998-2022 CERN for the benefit of the LHCb and ATLAS collaborations #
#                                                                                   #
# This software is distributed under the terms of the Apache version 2 licence,     #
# copied verbatim in the file "LICENSE".                                            #
#                                                                                   #
# In applying this licence, CERN does not waive the privileges and immunities       #
# granted to it by virtue of its status as an Intergovernmental Organization        #
# or submit itself to any jurisdiction.                                             #
#####################################################################################
from Gaudi.Configuration import *

import os
from DDDB.Configuration import GIT_CONDDBS
from Configurables import DeMagnetTester, CondDB, DDDBConf

# Main application
##################
app = ApplicationMgr(EvtSel="NONE", EvtMax=1, OutputLevel=INFO)

# DetDesc case, we let LHCbApp configure the services
from Configurables import LHCbApp, MagneticFieldSvc
LHCbApp().DataType = "Upgrade"
LHCbApp().Simulation = True
CondDB().Upgrade = True
DDDBConf.GeometryBackend = "DetDesc"
app.ExtSvc += [MagneticFieldSvc()]

# Configure fake run number
###########################
from Configurables import LHCb__Tests__FakeRunNumberProducer as FET
odin_path = '/Event/DummyODIN'
myalgs = [FET('FakeRunNumber', ODIN=odin_path, Start=42, Step=20)]

# Add our own algo
myalgs.append(DeMagnetTester('DeMagnetTester'))

# Set set the sequence
app.TopAlg = myalgs

import sys
print(sys.modules)
