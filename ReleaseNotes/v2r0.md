2021-10-25 Run2Support v2r0
===

This version uses 
LHCb [v53r1](../../../../LHCb/-/tags/v53r1),
Gaudi [v36r0](../../../../Gaudi/-/tags/v36r0) and
LCG [100](http://lcginfo.cern.ch/release/100/) with ROOT 6.24.00.

This version is released on `master` branch.
Built relative to Run2Support [v1r1](../../tags/v1r1), with the following changes:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Build | Rewrite CMake configuration in "modern CMake", !12 (@clemenci)
- Fix veloaligncond.updates test (follow lhcb/LHCb!3001), !15 (@rmatev)
- Dropped usage of (UN)LIKELY macro, !14 (@sponce)





