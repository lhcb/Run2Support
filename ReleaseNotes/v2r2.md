2022-08-29 Run2Support v2r2
===

This version uses 
LHCb [v53r9p1](../../../../LHCb/-/tags/v53r9p1),
Detector [v1r2](../../../../Detector/-/tags/v1r2),
Gaudi [v36r5](../../../../Gaudi/-/tags/v36r5) and
LCG [101](http://lcginfo.cern.ch/release/101/) with ROOT 6.24.06.

This version is released on `master` branch.
Built relative to Run2Support [v2r1](../../tags/v2r1), with the following changes:


### Fixes ~"bug fix" ~workaround

- ~"Event model" ~"Fast Simulations" | Fixed missing include in L0MuonError, !26 (@sponce)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~"Event model" ~"Fast Simulations" | Removed duplication of channelIDs between LHCb and Detector. Kept Detector ones, !27 (@sponce)
- ~Detector | Explicitly turn off MagneticFieldSvc in VeloAlignCond tests, !28 (@clemenci)
- ~Detector | Modified to use GeometryInfoPlus, !22 (@bcouturi)
- ~"Fast Simulations" | Rename Event/RecreatePIDTools to Event/Run2_RecreatePIDTools, !24 (@rmatev)


